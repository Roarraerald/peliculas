import 'package:http/http.dart' as http;

import 'dart:convert';

import 'package:peliculas/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = '8792caa43ae79e8924b602a9ce190c07';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';

  Future <List<Pelicula>> getEnCines() async {

  final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key': _apikey,
      'language': _language
    });

  final resp = await http.get(url);

  final decodedData = json.decode(resp.body);

  final peliculas = new Peliculas.fromJsonList(decodedData['results']);

  //print(decodedData['results']);

  //print(peliculas.items[0].title);


  //return [];
    return peliculas.items;
  }

}
